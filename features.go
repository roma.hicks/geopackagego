// Copyright 2020 Roma Hicks

// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

package geopackage

import (
	"bytes"
	"database/sql"
	"encoding/binary"
	"fmt"
	"log"
	"reflect"
	"regexp"
)

// LayerData is a structure for containing feature datasets
type LayerData struct {
	TableName string
	Columns   []*sql.ColumnType // List of the Columns found in the table.
	Rows      []*Row            // Actual row values.
	nullTypes []interface{}     // sql.NullTypes used for Scanning in rows.
}

// Row is a abstraction of data in a table's row.
// Row contains mostly accounting information for a Row's GeoPackage geometry metadata.
type Row struct {
	Data  []interface{} // Data in the actual row.  Needs to be type asserted for usage.
	SRSID int64         // SRS ID of the Row; unsure if this ever actually changes from the layer's SRS.
	Flags []byte        // GeoPackage Geometry Binary Header flags parsed out.
}

// ParseHeader parses the GeoPackage Geometry Binary Header flags and populates Row with
// geometry metadata.
func (r *Row) parseHeader(geoIndex int) {
	geometry := r.Data[geoIndex].([]byte)
	r.Flags = r.parseFlags(geometry[3])
	if r.Flags[7] == 1 {
		tt := binary.LittleEndian.Uint32(geometry[4:8])
		r.SRSID = int64(tt)
	} else {
		tt := binary.BigEndian.Uint32(geometry[4:8])
		r.SRSID = int64(tt)
	}
}

// parseFlags parses the flag byte in GeoPackage Binary Header.
// parseFlags parses the number four byte of GeoPackage SQL Geometry Binary Format
// so indivual bit are easily inspected.
func (r *Row) parseFlags(value byte) []byte {
	bitByteArray := make([]byte, 8)
	r.convertBitsToByteArray(&bitByteArray, 7, value)
	return bitByteArray
}

// convertBitsToByteArray recursively converts a byte into bit .
// NOTE: Not tested as it is exercised quite well in tests for parseFlags.
func (r *Row) convertBitsToByteArray(bitByteArray *[]byte, pos int, value byte) {
	if value < 1 || pos < 0 || value > 255 {
		return
	}
	(*bitByteArray)[pos] = value % 2
	r.convertBitsToByteArray(bitByteArray, pos-1, value/2)
}

// reworkGeometry scan the Row's geometry and removes the GeoPackage
// Binary Header and reassign the memory variable with the WKB only.
// https://www.geopackage.org/spec121/index.html#gpb_format
func (r *Row) reworkGeometry(geoIndex int) {
	geometryWithHeader := r.Data[geoIndex].([]byte)
	flags := r.Flags[4:7]
	// Skip ahead a number of bytes depending on Binary Header Metadata format.
	if bytes.Equal(flags, []byte{0, 0, 0}) {
		r.Data[geoIndex] = geometryWithHeader[8:]
	} else if bytes.Equal(flags, []byte{0, 0, 1}) {
		r.Data[geoIndex] = geometryWithHeader[40:]
	} else if bytes.Equal(flags, []byte{0, 1, 0}) || bytes.Equal(flags, []byte{0, 1, 1}) {
		r.Data[geoIndex] = geometryWithHeader[56:]
	} else if bytes.Equal(flags, []byte{1, 0, 0}) {
		r.Data[geoIndex] = geometryWithHeader[72:]
	} else {
		panic("Should never reach this error.")
	}
}

// LoadData loads the data from each table into the memory.
// This function loads all features in the GeoPackage into RAM. Large datasets
// and retrieving data from the database isn't supported at this time.
func (gp *GeoPackage) loadData(tableName string) (*LayerData, error) {
	layer := &LayerData{}
	layer.Rows = make([]*Row, 0)

	rows, err := gp.DB.Query(fmt.Sprintf("select * from %s;", tableName))
	defer rows.Close()
	if err != nil {
		return nil, err
	}

	log.Printf("GEOPACKAGE: SQL load data layer - %v\n", tableName)

	// Get column types and create template for scanning data into rows.
	// Lookup column names and append sql.NullTypes to store values from a Scan.
	// ScanType is not supported by SQLITE3 driver used.
	layer.Columns, err = rows.ColumnTypes()
	if err != nil {
		return nil, err
	}
	typeRegEx := regexp.MustCompile("^[A-Z]+")
	for _, v := range layer.Columns {
		typeStr := v.DatabaseTypeName()
		name := typeRegEx.FindString(typeStr)
		switch name {
		case "BOOLEAN", "TINYINT", "SMALLINT", "MEDIUMINT", "INT", "INTEGER":
			layer.nullTypes = append(layer.nullTypes, &sql.NullInt64{})
		case "FLOAT", "DOUBLE", "REAL":
			layer.nullTypes = append(layer.nullTypes, &sql.NullFloat64{})
		case "TEXT", "DATE", "DATETIME":
			layer.nullTypes = append(layer.nullTypes, &sql.NullString{})
		case "BLOB", "GEOMETRY", "POINT", "LINESTRING", "POLYGON", "MULTIPOINT", "MULTILINESTRING", "MULTIPOLYGON", "GEOMETRYCOLLECTION":
			b := make([]byte, 0)
			layer.nullTypes = append(layer.nullTypes, &b)
		default:
			log.Printf("GEOPACKAGE: Unkown column type encountered, %v. Using interface{}.", name)
			var i interface{}
			layer.nullTypes = append(layer.nullTypes, &i)
		}
	}

	// Load features from the database and create rows.
	for {
		newRow := &Row{}
		newRow.Data = make([]interface{}, 0)
		if rows.Next() != true {
			err = rows.Err()
			if err != nil {
				return nil, err
			}
			break
		}
		err = rows.Scan(layer.nullTypes...)
		if err != nil {
			return nil, err
		}
		typeCheckSlice := make([]byte, 0)
		for _, v := range layer.nullTypes {
			switch reflect.TypeOf(v) {
			case reflect.TypeOf(&sql.NullInt64{}):
				aNullInt64 := v.(*sql.NullInt64)
				newRow.Data = append(newRow.Data, aNullInt64.Int64)
			case reflect.TypeOf(&sql.NullFloat64{}):
				aNullFloat64 := v.(*sql.NullFloat64)
				newRow.Data = append(newRow.Data, aNullFloat64.Float64)
			case reflect.TypeOf(&sql.NullString{}):
				aNullString := v.(*sql.NullString)
				newRow.Data = append(newRow.Data, aNullString.String)
			case reflect.TypeOf(&typeCheckSlice):
				aByte := v.(*[]byte)
				newRow.Data = append(newRow.Data, *aByte)
			}
		}
		layer.Rows = append(layer.Rows, newRow)
	}
	geometryColumnName := sql.NullString{}
	geometryColumnNameRow := gp.DB.QueryRow(fmt.Sprintf("select column_name from gpkg_geometry_columns where table_name='%s';", tableName))
	err = geometryColumnNameRow.Scan(&geometryColumnName)
	if err != nil {
		log.Print(err)
	}
	if geometryColumnName.Valid != true {
		return layer, fmt.Errorf("geometry column was not found for table: %s", tableName)
	}
	for i, vc := range layer.Columns {
		if vc.Name() == geometryColumnName.String {
			for _, vr := range layer.Rows {
				vr.parseHeader(i)
				vr.reworkGeometry(i)
			}
		}
	}
	return layer, nil
}
