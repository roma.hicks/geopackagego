// WellKnownGeometry Test Suite by Roma Hicks is marked with CC0 1.0 Universal.
// To view a copy of this license, visit http://creativecommons.org/publicdomain/zero/1.0

package main

import (
	"testing"

	geopackage "codeberg.org/roma.hicks/geopackagego"
)

func TestOpenGeoPackage(t *testing.T) {
	t.Run("Open a GeoPackage and return object representing the database.", func(t *testing.T) {
		geodb, err := geopackage.OpenGeoPackage("test_points.gpkg")
		if err != nil || geodb == nil {
			t.Errorf("Failed to open GeoPackage: %v", err)
		}
		for _, v := range geodb.Content {
			t.Logf("Loaded tables:\n")
			t.Logf("  -- %v\n", v)
			if v.TableName == "test_poly" {
				for _, x := range v.Data.Rows {
					t.Logf("%v\n", x.Flags)
					t.Logf("%v\n", x.Data)
				}
			}
		}
	})
	t.Run("Open a file that is not a SQLITE3 database.", func(t *testing.T) {
		_, err := geopackage.OpenGeoPackage("fake.gpkg")
		if err == nil {
			t.Errorf("Should have returned an error but instead recieved a nil")
		}
	})
	t.Run("Open a file that is not a SQLITE3 database.", func(t *testing.T) {
		_, err := geopackage.OpenGeoPackage("sqlite_only.gpkg")
		if err == nil {
			t.Errorf("Should have returned an error but instead recieved a nil")
		}
	})
}
