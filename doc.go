// Copyright 2020 Roma Hicks

// This file is part of GeoPackageGo.

// GeoPackageGo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// GeoPackageGo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GeoPackageGo.  If not, see <https://www.gnu.org/licenses/>.

/*
Package geopackage is a Go package specifically for interacting with GeoPackage files.

A GeoPackage is a SQLite3 database that has been tailored to store geographical data,
common tables, and is extensible to support new features.

GeoPackage only supports read operations and loads all content in the GeoPackage into RAM.  It does not support R-tree indexing.

GeoPackages are opened using the OpenGeoPackage function which performs integrity checks of the database. If the specified file fails this checks it will still return the GeoPackage structure, but actual state of the database file will be unknown.

Once the package has been opened the data can be loaded into memory by using the LoadContent function. At the moment the entirety of the GeoPackage is loaded into RAM; if you are using a very limited resource machine or have large datasets this could lead to memory warnings. Once loaded, the layers can be accessed by through the GeoPackageGo data structure.
*/
package geopackage
